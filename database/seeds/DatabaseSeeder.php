<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $this->call(UserTableSeeder::class);
       $this->call(PropertySeeder::class);
       $this->call(PaymentMethodSeeder::class);
       $this->call(PaymentTypeSeeder::class);
       $this->call(TenantPaymentSeeder::class);
       $this->call(UnitPaymentSeeder::class);
       $this->call(UnitSeeder::class);
       $this->call(UnitTypeSeeder::class);
       $this->call(NotificationMessageSeeder::class);
       $this->call(NotificationSeeder::class);
       $this->call(NotificationTypeSeeder::class);
    }
}
