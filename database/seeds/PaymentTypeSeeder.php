<?php

use Illuminate\Database\Seeder;
use App\PaymentType;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentType::Truncate();
        PaymentType::insert([
                [
                    'name'=>'Lease Renewal',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'name'=>'Water',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'name'=>'Services charge',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]

        ]);
    }
}
