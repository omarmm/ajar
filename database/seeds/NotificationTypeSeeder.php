<?php

use Illuminate\Database\Seeder;
use App\NotificationType;
class NotificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         NotificationType::Truncate();
          NotificationType::insert([
                [
                    
                    'name'=>'early',
                    'receiver_type'=>'tenant',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    
                    'name'=>'due',
                    'receiver_type'=>'tenant',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    
                    'name'=>'late',
                    'receiver_type'=>'tenant',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                 [
                    
                    'name'=>'payment_completed',
                    'receiver_type'=>'landlord',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]


        ]);
    }
}
