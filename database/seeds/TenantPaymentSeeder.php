<?php

use Illuminate\Database\Seeder;
use App\TenantPayment;
class TenantPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         TenantPayment::Truncate();
        TenantPayment::insert([
                [
                    'tenant_id'=>'1',
                    'unit_id'=>'1',
                    'payments_type_id'=>'1',
                    'due_date'=>\Carbon\Carbon::now()->toDateTimeString(),
                    'amount'=>100,
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'tenant_id'=>'1',
                    'unit_id'=>'1',
                    'payments_type_id'=>'2',
                    'due_date'=>\Carbon\Carbon::now()->addDays(2)->toDateTimeString(),
                    'amount'=>100,
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'tenant_id'=>'1',
                    'unit_id'=>'1',
                    'payments_type_id'=>'3',
                    'due_date'=>\Carbon\Carbon::now()->subDays(2)->toDateTimeString(),
                    'amount'=>100,
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]

        ]);
    }
}
