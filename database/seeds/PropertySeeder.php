<?php

use Illuminate\Database\Seeder;
use App\Property;
class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        Property::Truncate();
        Property::insert([
                [
                    'name'=>'Al-Rehab Building',
                    'address'=>'Hawalli',
                    'description'=>'near to Dar al shefaa Hospital',
                    'landlord_id'=>'2',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'name'=>'AL-Ahmadi Building',
                    'address'=>'AL-Ahmadi',
                    'description'=>'description here',
                    'landlord_id'=>'2',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'name'=>'AL-Zahed Building',
                    'address'=>'Al Asimah',
                    'description'=>'Sahet al-Safah',
                    'landlord_id'=>'2',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]

        ]);
    }
}
