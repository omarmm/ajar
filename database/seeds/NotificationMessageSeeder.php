<?php

use Illuminate\Database\Seeder;
use App\NotificationMessage;
class NotificationMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          NotificationMessage::Truncate();
          NotificationMessage::insert([
                [
                    
                    'status'=>'early',
                    'message'=>' Dear @user_name , kindly be informed that your @payment_name invoice will be issued in @due_date.',
                    'receiver_type'=>'tenant',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    
                    'status'=>'due',
                    'message'=>'Dear @user_name ,  Your @payment_name invoice has been issued. Please visit this link to complete your payment. @link',
                    'receiver_type'=>'tenant',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    
                    'status'=>'late',
                    'message'=>'Dear @user_name ,  you have missed your last monthly @payment_name payment. Please visit this link to complete your payment.  @link',
                    'receiver_type'=>'tenant',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],

                [
                  
                    'status'=>'late',
                    'message'=>'Dear Landlord , tenant @user_name has completed his @payment_name payment successsfully. Please visit this link for more informations.  @link',
                    'receiver_type'=>'landlord',
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]

        ]);
    }
}
