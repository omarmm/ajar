<?php

use Illuminate\Database\Seeder;
use App\UnitPayment;
class UnitPaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         UnitPayment::Truncate();
        UnitPayment::insert([
                [
                   
                    'unit_id'=>'1',
                    'payment_type_id'=>'1',
                    'amount'=>500,
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                 
                    'unit_id'=>'1',
                    'payment_type_id'=>'2',
                    'amount'=>100,
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                  
                    'unit_id'=>'1',
                    'payment_type_id'=>'3',
                    'amount'=>100,
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]

        ]);
    }
}
