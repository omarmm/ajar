<?php

use Illuminate\Database\Seeder;
use App\Unit;
class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Unit::Truncate();
        Unit::insert([
                [
                    'property_id'=>'1',
                    'number'=>'1',
                    'floor'=>'1',
                    'unit_type_id'=>'2',
                    'rent_amount'=>100,
                    'tenant_id'=>'1',
                    'rented_from_date'=>\Carbon\Carbon::now()->toDateTimeString(),
                    'rented_to_date'=>\Carbon\Carbon::now()->addMonths(1)->toDateTimeString(),
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'property_id'=>'1',
                    'number'=>'2',
                    'floor'=>'1',
                    'unit_type_id'=>'2',
                    'rent_amount'=>100,
                    'tenant_id'=>'1',
                    'rented_from_date'=>\Carbon\Carbon::now()->toDateTimeString(),
                    'rented_to_date'=>\Carbon\Carbon::now()->addMonths(1)->toDateTimeString(),
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'property_id'=>'2',
                    'number'=>'3',
                    'floor'=>'2',
                    'unit_type_id'=>'2',
                    'rent_amount'=>100,
                    'tenant_id'=>'1',
                    'rented_from_date'=>\Carbon\Carbon::now()->toDateTimeString(),
                    'rented_to_date'=>\Carbon\Carbon::now()->addMonths(1)->toDateTimeString(),
                    'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                    'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()
                ]

        ]);
    }
}
