<?php

use Illuminate\Database\Seeder;
use App\Notification;
use App\NotificationQueue;
class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Notification::Truncate();
        NotificationQueue::Truncate();
    }
}
