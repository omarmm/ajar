<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
      DB::table('users')->Truncate();
      $users = [
                ['name' => 'tenant user',
                'username' => 'tenant',
                'type' =>'tenant',
                'password' => Hash::make('123456789'),
                'email'   => 'eng.omarmm@gmail.com',
                'mobile' => '+201002848469',
                'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()] ,



                ['name' => 'landlord user',
                'username' => 'landlord',
                'type' =>'landlord',
                'password' => Hash::make('123456789'),
                'email'   => 'omar.portal@psu.edu.eg',
                'mobile' => '+201002848469',
                'created_at' =>\Carbon\Carbon::now()->toDateTimeString(),
                'updated_at'=>\Carbon\Carbon::now()->toDateTimeString()]

             ];

        foreach($users as $user){
         DB::table('users')->insert($user);
        }
      
    }
}
