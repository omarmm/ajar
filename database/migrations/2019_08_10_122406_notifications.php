<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_payment_id');
            $table->string('message');
            $table->integer('notification_type_id');
            $table->tinyInteger('is_sent_push')->default(0);
            $table->tinyInteger('is_read_push')->default(0);
            $table->tinyInteger('is_sent_email')->default(0);
            $table->tinyInteger('is_read_email')->default(0);
            $table->tinyInteger('is_sent_sms')->default(0);
            $table->tinyInteger('is_read_sms')->default(0);
            $table->timestamps();
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
   Schema::dropIfExists('notifications');

    }
}
