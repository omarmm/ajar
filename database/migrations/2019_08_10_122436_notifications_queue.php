<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotificationsQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //notification_id

        Schema::create('notifications_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notification_id');
            $table->string('type'); //sms //push  //Email
            $table->timestamps();
           // $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('notifications_queue');
    }
}
