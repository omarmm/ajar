<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Units extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->integer('tenant_id');
            $table->integer('number');
            $table->integer('floor');
            $table->integer('unit_type_id');
            $table->timestamp('rented_from_date')->nullable();
            $table->string('rental_period')->default('monthly');
            $table->timestamp('rented_to_date')->nullable();
            $table->decimal('rent_amount',11);
            $table->string('status')->default('rented');
            $table->timestamps();
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');

    }
}
