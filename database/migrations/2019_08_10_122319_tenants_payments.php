<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TenantsPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('unit_id');
            $table->integer('payments_type_id');
            $table->timestamp('due_date')->nullable();
            $table->timestamp('paymet_date')->nullable();
            $table->decimal('amount',11);
            $table->string('period')->default('monthly');
            $table->tinyInteger('is_paid')->default(0);
            $table->integer('payment_method_id')->nullable();
            $table->timestamps();
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants_payments');
    }
}
