<?php
use App\User;
use App\TenantPayment;
use App\Notification;
use App\NotificationQueue;
use App\Libraries\TwilioSmsService;
use App\Services\NotificationService;
use App\Mail\EmailNotification;
use \Carbon\Carbon;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/invoice/', 'InvoiceController@show')->name('invoice');

Route::patch('/invoice/confirm/{id}', 'InvoiceController@confirm')->name('invoice.confirm');

Route::get('/users/profile/{id}', 'UserController@profile')->name('user.profile');

Route::patch('/users/update/{id}', 'UserController@update')->name('user.update');





