<?php
use App\Libraries\Helpers;
use App\User;
use \Carbon\Carbon;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$router->group(['middleware' => 'apiauth'], function () use ($router) {

	$router->post('/account_notifications',  ['uses' => 'ApiController@account_notifications']);
});


$router->post('/login',  ['uses' => 'ApiController@login']);