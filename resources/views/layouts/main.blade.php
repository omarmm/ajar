<link href="{{ asset('css/invoice.css') }}" media="all" rel="stylesheet" type="text/css" />

@yield('content')

<script type="text/javascript" src="{{ asset('js/invoice.js') }}"></script>
