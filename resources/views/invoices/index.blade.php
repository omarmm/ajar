 @extends('layouts.main') 

 @section('content')


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--Author      : @arboshiki-->

<div id="invoice">

    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
        </div>
        <hr>
    </div>
    <div class=" invoice invoice-box">
        @if($payment->is_paid==1)
<div class="alert alert-success" role="alert">
 this invoice has been paid successfully
</div>

@endif
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                              AJAR
                            </td>
                            
                            <td>
                                Invoice #: {{$payment->id}}<br>
                                Created: {{$payment->DueDateView}}<br>
                                Due: {{$payment->DueDateView}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <b>Unit Detalis:</b><br>
                                building name: {{$payment->unit->property->name}}<br>
                                address: {{$payment->unit->property->address}}<br>
                                unit {{$payment->unit->number}}, floor {{$payment->unit->number}}
                            </td>
                            
                            <td style="text-align: left;padding-left: 150">
                                Invoice to : {{$payment->tenant->name}}<br>
                                Email:       {{$payment->tenant->email}}<br>
                                Mobile:      {{$payment->tenant->mobile}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
           <!--  <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    credit card #
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    credit card
                </td>
                
                <td>
                    1000
                </td>
            </tr> -->
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td>
                   {{$payment->type->name}}
                </td>
                
                <td>
                   {{$payment->amount}} K.D
                </td>
            </tr>
            
           
            
            
            <tr class="total">
                <td></td>
                
                <td>
                   Total: {{$payment->amount}} K.D
                </td>
            </tr>

          
        </table>
        @if($payment->is_paid!=1)

          <form method="POST" action="{{route('invoice.confirm',['id' => $payment->id])}}">
                @csrf
                @method('PATCH')
                <button type="submit" class="btn btn-info"><i class="fa fa-print"></i> confirm payment</button>
            </form>

            @endif

    </div>
</div>

 @endsection