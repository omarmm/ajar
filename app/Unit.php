<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //relations

   
      public function property()
    {
        return $this->belongsTo('App\Property');
    }


      public function tenant()
    {
        return $this->belongsTo('App\User', 'tenant_id');
    }
}
