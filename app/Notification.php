<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	protected $fillable = [
        'is_sent_email', 'is_read_email', 
        'is_sent_push', 'is_read_push',
        'is_sent_sms', 'is_read_sms',
    ];
    //relations
     public function tenantPayment()
    {
        return $this->belongsTo('App\TenantPayment');
    }


   public function queue()
    {
       return $this->hasMany('App\NotificationQueue');
    }





    //Scopes

     public function scopeRecent($query)
    {
        return $query->where([
               ['is_sent_sms', 1],
               ['is_sent_email',1]]);
    }

      public function scopeUpcoming($query)
    {
        return $query->where([
               ['is_sent_sms', 0],
               ['is_sent_email',0]])->whereHas('queue', function($q) {
               });
    }
}
