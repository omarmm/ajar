<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\TenantPayment;
use App\Notification;
use App\NotificationQueue;
use App\Libraries\TwilioSmsService;
use App\Services\NotificationService;
use \Carbon\Carbon;

class SetNotificationCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SetNotification:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        \Log::info("SetNotificationCron is working fine!");
          $now = Carbon::now();

          $tenants = User::where('type','tenant')->get();
           foreach ($tenants as $tenant ){
       
                  foreach($tenant->payments as $payment){ 
       
                   $date = $payment->due_date; 
                   $diff = $now->diffInHours($date , false);
                   $message_id=0;
                    if($diff < 48 && $diff > 0){
       
                       $message_id= 1; //early
       
                    }elseif($diff <= 0 && $diff > -24) {
                       $message_id= 2; //due
       
                    }elseif($diff < -24) {
                    
                       $message_id= 3; //late
                          
                    }else{
                         continue;
                        }
       
                      $config = [
                                'user_type' => 'tenant',
                                'message_id'  => $message_id,
                                'tenant_payment_id'=>$payment->id
                              
                            ];
                    
                     $PaymentNotification= new NotificationService($config);
                     $PaymentNotification->set();
                    
                    }
       
             }

             $this->info('SetNotification:cron Cummand Run successfully!');
    }
}
