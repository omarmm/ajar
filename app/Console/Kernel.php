<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\EmailNotificationQueue;
use App\Jobs\SMSNotificationQueue;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\SetNotificationCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {          // set notifications
               $schedule->command('SetNotification:cron')
                 ->everyMinute();
              // send early type notifications every minute (1 = early type)
               $schedule->job(new EmailNotificationQueue(1))->everyMinute();
              // $schedule->job(new SMSNotificationQueue(1))->everyMinute();

              // send due type notifications 10 minutes (2 = due type)
               $schedule->job(new EmailNotificationQueue(2))->everyTenMinutes();
              // $schedule->job(new SMSNotificationQueue(2))->everyTenMinutes();

                 // send due type notifications 5 minutes (3 = late type)
               $schedule->job(new EmailNotificationQueue(3))->everyFiveMinutes();
              // $schedule->job(new SMSNotificationQueue(3))->everyFiveMinutes(); /



                 // send due type notifications  minute (4 = sent payment confirmed to landlord)
               $schedule->job(new EmailNotificationQueue(4))->everyMinute();
               $schedule->job(new SMSNotificationQueue(4))->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
