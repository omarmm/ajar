<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Validator;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     private $rules = array(
    'email' => 'required|email',
    'name' => 'required|min:6',
    'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/'
  );

    //relations


    public function units()
    {
       return $this->hasMany('App\Unit' ,'tenant_id');
    }


    public function properties()
    {
       return $this->hasMany('App\Property' ,'landlord_id');
    }


    public function token()
    {
        return $this->hasOne('App\UserPushToken');
    }


    public function payments()
    {
     return $this->hasMany('App\TenantPayment', 'tenant_id')->where('is_paid',0);

    }

     public function invoices()
    {
     return $this->hasMany('App\TenantPayment', 'tenant_id')->where('is_paid',1);

    }


    //validation function

    public function validate($input) {
    return Validator::make($input, $this->rules);
  }



}
