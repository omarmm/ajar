<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Libraries\Helpers;
use Illuminate\Contracts\Auth\Factory as Auth;

class ApiAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next, $guard = null)
    {

         $access_token  = $request->header('access-token');

         $requesto = (array)json_decode($request->getContent(), true);
        if(array_key_exists('lang_id',$requesto)) {
            Helpers::Set_locale($requesto['lang_id']);
        }
        if($access_token)
             {
                $access_token=$access_token;
           
            
            if ($access_token) 
            {   $user= User::where('api_token', $access_token)->first();
             }
              if ($user) 
            {   
                  return $next($request);
              }else{

                return Helpers::Get_Response(400,'error',trans('messages.logged'),[],[]);
              }
              
            }  
        return Helpers::Get_Response(400,'error',trans('messages.logged'),[],[]);
    }
}
