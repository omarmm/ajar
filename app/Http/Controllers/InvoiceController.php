<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TenantPayment;
use App\Notification;
use App\Services\NotificationService;



class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
         $payment_id =      $request->get('p_id');
         $notification_id = $request->get('n_id');
         $notification_type = $request->get('type');
         $data['payment'] = TenantPayment::find($payment_id);
         $notification_is_read =  Notification::find($notification_id);

         if($notification_type == 'email'){
            $notification_is_read->update(['is_read_email' => 1]);
         }elseif($notification_type == 'sms'){
            $notification_is_read->update(['is_read_sms' => 1]);
         }elseif($notification_type == 'push'){
            $notification_is_read->update(['is_read_push' => 1]);
         }

         return view('invoices.index' , $data);
    }


      public function confirm($id)
    {

          $payment = TenantPayment::find($id);
          if($payment->is_paid==0){
          $update =  $payment->update(['is_paid' => 1]);
          if($update){
          $config = [ 'user_type' => 'landlord',
                      'message_id'  => 4,  // 4 = landlord payment confirmed
                      'tenant_payment_id'=>$payment->id];
                    
                     $PaymentNotification= new NotificationService($config);
                     $PaymentNotification->set();

               }      

               }      
       
        return redirect()->back();

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
