<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Helpers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Notification;
use \Carbon\Carbon;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     //   $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function login(Request $request)
    {
        $request = (array)json_decode($request->getContent(), true);

        $validator = Validator::make($request, [
            "mobile" => "required",
            "password" => "required|min:8|max:20",

        ]);
        if ($validator->fails()) {
            return Helpers::Get_Response(403, 'error', '', $validator->errors(), []);
        }
 
            if (is_numeric($request['mobile'])) {
        $user = User::where("mobile", "=", $request['mobile'])->first();

                if (!$user) {
                    return Helpers::Get_Response(400, 'error', trans('mobile isn’t registered'), $validator->errors(), []);
                }
            }else{
                 return Helpers::Get_Response(400, 'error', trans('invalid mobile number'), $validator->errors(), []);
            }

            if ($user) {
                if (Hash::check($request['password'], $user->password)) {
                    if ($user->is_active == 1) {
                        $tokenobj = $user->createToken('api_token');
                        $token = $tokenobj->accessToken;
                        $token_id = $tokenobj->token->id;
                        $user->api_token = $token_id;
                        $user->created_at = Carbon::now()->format('Y-m-d H:i:s');
                        $user->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                        $user->last_login = Carbon::now()->format('Y-m-d H:i:s');
                        if(array_key_exists('device_token',$request)){
                            if($request['device_token'] != ''){
                                $user->device_token = $request['device_token'];
                            }
                        }
                        $user->save();


                        return Helpers::Get_Response(200, 'success', '', $validator->errors(), $user);
                    } else {
                        return Helpers::Get_Response(402, 'error', trans('user not active'), $validator->errors(), []);
                    }
                }
                return Helpers::Get_Response(400, 'error', trans('wrong password'), $validator->errors(), []);
             }
          
      
    }


    public function account_notifications(Request $request){


     // notification has payment has user

        $api_token = $request->header('access-token');

         $recent = count(Notification::whereHas('tenantPayment', function($q ) use($api_token)  {
               $q->whereHas('tenant', function($q2) use($api_token) {
               $q2->where('api_token', $api_token);
               });})->Recent()->get());

         $upcoming = count(Notification::whereHas('tenantPayment', function($q) use($api_token)  {
               $q->whereHas('tenant', function($q2) use($api_token) {
               $q2->where('api_token', $api_token);
               });})->Upcoming()->get());

         $notifications = ['recent_notifications'=>$recent ,

                            'upcoming_notifications'=>$upcoming  ];

   

        return Helpers::Get_Response(200, 'success', '', '',$notifications);
    }
}
