<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;


class TenantPayment extends Model
{
    protected $table = 'tenants_payments';

        protected $fillable = [
        'is_paid'
    ];


    //relations
     public function tenant()
    {
        return $this->belongsTo('App\User' , 'tenant_id');
    }

      public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }

      public function type()
    {
        return $this->belongsTo('App\PaymentType' ,'payments_type_id'
    );
    }

    //attributes

    public function getDueDateViewAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date)->format('d. M, Y');
    }


}
