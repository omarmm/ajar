<?php

namespace App\Services;
use App\TenantPayment;
use App\Notification;
use App\NotificationQueue;
use App\NotificationMessage;


class NotificationService
{
	protected $user_type = null;
	protected $message_id   = null;
	protected $tenant_payment_id = null;
	protected $notifications_types = null;
    //protected $message   = null;

	public function __construct($config)
	{
		$this->user_type = $config['user_type'];
		$this->message_id   = $config['message_id'];
        $this->tenant_payment_id   = $config['tenant_payment_id'];
	    $this->notifications_types = ['push' , 'email' , 'sms' ];
	}

	public function set()
	{

		$tenant_payment = TenantPayment::find($this->tenant_payment_id);
          // first check of notification exist
        $notification_exist = Notification::where('tenant_payment_id' ,$tenant_payment->id)
        ->where('notification_type_id' ,$this->message_id)
        ->first();
       // dd(count($notification_exist));
        if(count($notification_exist)<1){
         $message =NotificationMessage::find($this->message_id)->message; 
         $user_name =$tenant_payment->tenant->name;
         $last_notification = Notification::orderBy('id', 'desc')->first();
         $currentId = (count($last_notification)>0)?$last_notification->id+1:1;
         $link =  url("invoice?p_id={$tenant_payment->id}&n_id={$currentId}");
         $vars = array($tenant_payment->type->name, $tenant_payment->DueDateView , $user_name ,$link);
         $message =  $this->bind_varaibles_inString($vars , $message);
         // Notifications (use database Transactions here)
         $notification = new Notification;
         $notification->tenant_payment_id  = $tenant_payment->id; //dd($tenant_payment->id);
         $notification->message = $message;
         $notification->notification_type_id = $this->message_id;
         $notification->created_at = \Carbon\Carbon::now()->toDateTimeString();
         $notification->updated_at = \Carbon\Carbon::now()->toDateTimeString();
         $notification->save(); 
       
         //notification queue
         foreach ($this->notifications_types as $type) {
         $queue = new NotificationQueue;
         $queue->notification_id = $notification->id;
         $queue->type = $type;
        // $queue->push_token =  $tenant->token->token;
         $queue->created_at = \Carbon\Carbon::now()->toDateTimeString();
         $queue->updated_at = \Carbon\Carbon::now()->toDateTimeString();
         $queue->save();

       }

       }
	}

   public function bind_varaibles_inString($vars , $text){

    $text = str_replace('@payment_name', $vars[0], $text);
    $text = str_replace('@due_date', $vars[1], $text);
    $text = str_replace('@user_name', $vars[2], $text);
    $text = str_replace('@link', $vars[3], $text);
    return $text;
    
    
    }

}
