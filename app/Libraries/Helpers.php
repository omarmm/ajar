<?php


namespace App\Libraries;
use DB;
class Helpers
{


  public static  function Get_Response($code , $message , $error_details , $validation_errors , $content) {
    $validation = [];
    $i = 0;
    $validation_errors = current((array) $validation_errors);
    if(is_array($validation_errors) && sizeof($validation_errors) != 0) {
        foreach($validation_errors as $key=>$value) {
            $validation[$i]['field']=$key;
            $validation[$i]['message']=$value;
            $i++;
        }
    }
    return response()->json(['status'=>['code'=>$code,'message'=>$message,'error_details'=>$error_details,'validation_errors'=>$validation],'content'=>$content],200,[],JSON_UNESCAPED_UNICODE);


  }


    public static function Set_locale($locale)
  {
    if($locale == 1)
      {
        app('translator')->setLocale('en');
      }
     else  if($locale == 2)
      {
        app('translator')->setLocale('ar');
      }
  }

}