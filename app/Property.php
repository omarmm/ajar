<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //relations
    public function landlord()
{
    return $this->belongsTo('App\User', 'landlord_id');
}

}
