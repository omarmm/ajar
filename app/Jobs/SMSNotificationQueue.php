<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notification;
use App\NotificationQueue;
use App\Libraries\TwilioSmsService;
use \Carbon\Carbon;
use Log;

class SMSNotificationQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     protected $notification_type = null;
    public function __construct($notification_type)
    {
         $this->notification_type = $notification_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
             $queue_smss = NotificationQueue::where('type','sms')->whereHas('notification', function($q) {
               $q->where('notification_type_id', $this->notification_type);
               })->limit(20)->get(); 

          foreach ($queue_smss as $queue_sms ){    
          
          $to_mobile =($this->notification_type==4)?$queue_sms->notification->tenantPayment->unit->property->landlord->mobile:$queue_sms->notification->tenantPayment->tenant->mobile;
         
           $twilio_config = [
            'app_id' => 'ACe53b0fe4aad76570e6f4f04e7427a5ae',
            'token'  => '3ec1837241c758603bfe7b4bdd8a4a1c',
            'from'   => '+12055513976'
        ];

        $twilio = new TwilioSmsService($twilio_config);
        $send =$twilio->send($to_mobile,$queue_sms->notification->message.'&type=sms');
        
          if ($send['status'] ==1) {
        
           Notification::find($queue_sms->notification->id)->update(['is_sent_sms' => 1]);
           $queue_sms->delete();
           }

         }


            }catch(\Exception $e){
        Log::error($e->getMessage());
            }
    }
}
