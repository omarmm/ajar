<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notification;
use App\NotificationQueue;
use App\Mail\EmailNotification;
use \Carbon\Carbon;
use Mail;
use Log;


class EmailNotificationQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void


     */
    protected $notification_type = null;
    public function __construct($notification_type)
    {
        $this->notification_type = $notification_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {  try{
          $queue_mails = NotificationQueue::where('type','email')->whereHas('notification', function($q) {
               $q->where('notification_type_id', $this->notification_type);
               })->limit(20)->get(); 

          foreach ($queue_mails as $queue_mail ){    
          $to_name = ( $this->notification_type==4)?$queue_mail->notification->tenantPayment->unit->property->landlord->name:$queue_mail->notification->tenantPayment->tenant->name;
          $to_email =($this->notification_type==4)?$queue_mail->notification->tenantPayment->unit->property->landlord->email:$queue_mail->notification->tenantPayment->tenant->email;
          $data = array("name"=>$to_name, "body" => $queue_mail->notification->message.'&type=email');
          $email_notification = new EmailNotification($data);
          $send =  Mail::to($to_email)->send($email_notification);
        
          if (!Mail::failures()) {
        
           Notification::find($queue_mail->notification->id)->update(['is_sent_email' => 1]);
           $queue_mail->delete();
           }
             
           }

            }catch(\Exception $e){
        Log::error($e->getMessage());
            }
           // NotificationQueue::Truncate();
    }
}
