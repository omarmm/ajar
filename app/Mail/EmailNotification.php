<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        //
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         // Array for Blade
    $input = array(
                      'name'     => $this->mailData['name'],
                      'body'     => $this->mailData['body'],
                  );

    return $this->view('emails.mail')
                 ->from('omar.ebrahim1200@gmail.com', 'Ajar')
                 ->subject('Ajar')
                // ->replyTo('hello@q-software.com', 'Q Software')
                ->with([
                    'inputs' => $input,
                  ]);



    }
}
