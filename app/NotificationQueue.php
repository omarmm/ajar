<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationQueue extends Model
{
    //
    protected $table = 'notifications_queue';


    //relations

    public function notification()
    {
        return $this->belongsTo('App\Notification');
    }


     public function scopeEarly($query)
    {
        return $query->whereHas('notification', function($q) {
               $q->where('notification_type_id', 1);
               });
    }
}
