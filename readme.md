**What is this Project ?**

This project is a Demo for notification system that will be used to notify both landlords and tenants. With the main two types of notification : 
 Event-based 
& Scheduled based  

*  The tenant will receive a scheduled reminder regarding his invoices early / due / late based on invoice due date.
*  The landlord will receive a notification when a tenant completes a payment.

There are two types of notifications applied :

* Email
* SMS

 (Android push notification will be added soon)
 
 
 
 
** Getting started**


1.  clone the project


`git clone https://gitlab.com/omarmm/ajar.git`

2. Run composer install


`composer install`


3. create your database and add set mysql configuration in the .env file then run the database migration command:


`php artisan migrate`


4. Now you will fill the database tables with basic data by running this command:


`php artisan db:seed`


5. Run `php artisan serve`


6. In the web browser copy these links :

`http://127.0.0.1:8000/users/profile/1`(tenant user)

`http://127.0.0.1:8000/users/profile/2` (landlord user)

These links for Tenant and landlord (basic static accounts ) ,
you should update users data (specially with valid email and mobile number) so that you can recieve the notifications .

7. in .env file you should set mail account configurations , this email will be used by laravel Mailable class and notifications 
will be sent throuth this email account .. configuration will be like the following:

>  MAIL_DRIVER=smtp

>  MAIL_HOST=smtp.gmail.com

>  MAIL_PORT=587

>  MAIL_USERNAME=username@gmail.com

>  MAIL_PASSWORD=secret

>  MAIL_ENCRYPTION=tls


8. now we will run tasks schedule .. in the data base there are a static tenant payments records (for example : lease renewal , water invoice ...)
``
 and there is a SetNotification command this command will check for these payments .. is they are new added payment this SetNotification command
will generate new notifications.

there are 4 types of notifications (3 types for sheduled notifications early / due / late ) 

and one for event notification  (The one the landlord will received  when a tenant completes a payment).

SetNotification command works in this way:


1- early notification: checks if  payment due date after just 48 hours , then notification will be generated and in notification queue


three types will be added (email , sms  , push notification).


2- due type notification will be set within 24 hours "if current date equal payment due date"


3- late type notification will be set after due date by 24 hours or more.


9. if you use linux base operating system like UBUNTU .. you can add the following cron tab command:

 `* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1`
 
 if you don't you will run this command :
 
 `php artisan schedule:run`

I have set queued jobs (EmailNotificationQueue , SMSNotificationQueue) , based on notification type (early / due  / late)


and for testing purpose I have scheduled these jobs to send each notification type in specific cron time .. for example



check for early notifications and send them every minute 



check for due notifications and send them every 5 minutes



check for late notifications and send them every 10 minutes



>  you can check app/Console/Kernel.php       



10. run queue work command to process the queued jobs:


`php artisan queue:work`


11. now check your mobile and email (you can wait for about 15 minutes to make sure that all notifications types have recieved )


12. click on the link in the email (make sure that you are runing `php artisan serve` ) 


13. the link will redirect you to payment invoice .. in the bottom of the page there is "confirm payment" button


click on it to generate landlord payment confirmed notification (make sure that these 2 commands are rununig to precess this notification:)

 `* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1`
 
`php artisan queue:work`




**API**:

First please check API collection : [API collection](https://documenter.getpostman.com/view/2285470/SVfGyC49?version=latest)

14. run `php artisan passport:install`

An API that will return the stats of the account notification the api will return the following:

● Number of recent notifications 


● Number of upcoming notifications ( for the scheduled notification) 


15. you must login first with the registered mobile number and password "123456789"

16. copy "api_token" in the response

17. copy it in account_notifications End point's header variable "access-token"



**Question 3**
 If the SMS / email provider doesn’t provide stats or data on who opens/ receives notifications. How would you keep track of this kind of data? 
 
 
 I have added is_sent and is_read flags for each notification type (email , SMS , push notification)..
 
 = when notification sent successfully is_sent flag will updated to 1
 
 = when user opens invoice link from email or sms is_read_sms or is_read_email flag will updated to 1
 
 
 
 
** Thank you**
********










